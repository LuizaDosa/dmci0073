package dmci0073MV.control;

import dmci0073MV.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;



public class BibliotecaCtrlTestBBT {
  Carte c10;
  BibliotecaCtrl ctrl;


    private List<String> autoriCarte=new ArrayList<>(
            Arrays.asList("Rebreanu","Creanga"));

    private List<String> cuvinteCheie=new ArrayList<>(
            Arrays.asList("broscuta","lup"));




    @Before
    public void setUp() throws Exception {
    c10=new Carte();
   ctrl=new BibliotecaCtrl();


    }

    @After
    public void tearDown() throws Exception {

    }



  //Test-case-uri realizate (4 cu date valide , 4 cu date non-valide)
    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteLugimeTitluValid() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"1900","Litera",cuvinteCheie);

//        c10.setAutori(autoriCarte);c10.setCuvinteCheie(cuvinteCheie);
//        System.out.println(autoriCarte);
        //ctrl=new BibliotecaCtrl();
      //c10.setTitlu("Povesti");
      //assertEquals(7,c10.getTitlu().length());
       /// assertEquals("1900",c10.getAnAparitie());
      //  assertEquals("Litera",c10.getEditura());

            ctrl.adaugaCarte(c10);


    }
    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteAnAparitieValid() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"1950","Litera",cuvinteCheie);
//        //assertEquals("1650",c10.getAnAparitie());
//        ctrl=new BibliotecaCtrl();
//        assertEquals( 1950,Integer.parseInt(c10.getAnAparitie()));
//        assertEquals("Povesti",c10.getTitlu());
//        assertEquals("Litera",c10.getEditura());
//        try {
//            ctrl.adaugaCarte(c10);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        assert true;
        ctrl.adaugaCarte(c10);


    }

    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteLungimeEdituraValid() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"1900","Litera",cuvinteCheie);
//        ctrl=new BibliotecaCtrl();
//        assertEquals(6,c10.getEditura().length());
//        assertEquals("Povesti",c10.getTitlu());
//        assertEquals("1900",c10.getAnAparitie());
        ctrl.adaugaCarte(c10);
    }

    @Test(expected = Exception.class)
    //date valide
    public void adaugaCarteAnValid() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"2018","Litera",cuvinteCheie);
       // assertEquals( 2018,Integer.parseInt(c10.getAnAparitie()));
        ctrl.adaugaCarte(c10);
    }


    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteAnInvalid() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"1700","Litera",cuvinteCheie);

       // assert (Integer.parseInt(c10.getAnAparitie()) < 1800);   //aici trece testul
       // throw new Exception("Anul este invalid");
        // aici da mesaj de exceptie
        ctrl.adaugaCarte(c10);
    }

    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteAnInvalidBVA() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"2019","Litera",cuvinteCheie);
//        assert (Integer.parseInt(c10.getAnAparitie()) > 2018);
//        throw new Exception("Anul este invalid");
        ctrl.adaugaCarte(c10);
    }

    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteEdituraInvalida() throws Exception {
        c10=new Carte("Povesti",autoriCarte,"1900","Lit",cuvinteCheie);
//        assertEquals(3,c10.getEditura().length());
////        assertEquals("Povesti",c10.getTitlu());
////        assertEquals("1900",c10.getAnAparitie());
        ctrl.adaugaCarte(c10);

    }
    @Test(expected = Exception.class)
    //date invalide
    public void adaugaCarteTiluInvalid() throws Exception{
        c10=new Carte("Povvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv",autoriCarte,"1900","Lit",cuvinteCheie);
//        assertEquals(101 ,c10.getTitlu().length());
//        assertEquals("1900",c10.getAnAparitie());
//        assertEquals("Lit",c10.getEditura());
//        throw new Exception("Mai mult de 100 caractere pentru titlu");
        ctrl.adaugaCarte(c10);

    }





    }


