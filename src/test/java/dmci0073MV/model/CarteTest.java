package dmci0073MV.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {

    Carte c, c1;
    Carte carte = new Carte();

    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("Povesti");
        c.setEditura("Litera");

        c.adaugaAutor("Rebreanu");


        System.out.println("in BeforeTest");
    }

    @After
    public void tearDown() throws Exception {
        c = null;
        System.out.println("in AfterTest");
    }

    @BeforeClass //configurarile vor fi valabile pt toate testele; cealalata cu Before se reseteaza dupa fiecare test
    public static void setup() throws Exception {

        System.out.println("before any test");
    }

    @AfterClass
    public static void teardown() throws Exception {

        System.out.println("after all tests");
    }


    @Test
    public void getTitlu() throws Exception {
        assertEquals("titlu=Povesti", "Povesti", c.getTitlu());
        // assertTrue("Povesti",true);
    }


    @Test
    public void setTitlu() throws Exception {
        c.setTitlu("Povestiri");
        assertEquals("Povestiri", c.getTitlu());
    }

    @Test(timeout = 100)
    public void getEditura() {

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertEquals(c.getEditura(), "Litera");
        }
    }


    @Test(expected = NullPointerException.class)
    //tb sa arunce exceptie ca sa treaca testul. simulam comportament care sa arunce exceptie
    public void testConstructor() {
        assert c1 == null;
        //c1 nu este initializat ci doar declarat
        throw new NullPointerException();
    }

    @Test(expected = Exception.class)
    public void setAnAparitie() throws Exception {
        c.setAnAparitie("1899");   //an <1900 in test, tb sa treaca. Testul il trece daca are de aruncat exceptie
    }


    @Test
    public void getEditura1() throws Exception {
        assertEquals("Litera", c.getEditura());
    }


    @Test(expected = NullPointerException.class)
    //tb sa arunce exceptie ca sa treaca testul. simulam comportament care sa arunce exceptie
    public void testCuvinteCheie() {
        if (c.getCuvinteCheie() != null)  //ca sa pice testul ==null
            throw new NullPointerException();
    }


    @Test(timeout = 50)
    public void testCautaDupaAutor() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertFalse(c.cautaDupaAutor("Eminescu"));
        }
    }

    @Test
    public void testDelete() throws Exception {

        c.deleteAutor("Rebreanu");
        //    assertEquals("Rebreanu", c.getAutori()); //aici e failed pt ca am sters dar lista mea mai are alti autori
        assertFalse(c.cautaDupaAutor("Rebreanu"));
    }


//
//    @Test(expected = Exception.class) // aici face inainte validarea cea mare?
//    public void setAnAparitie2() throws Exception {
//         c.setAnAparitie("ABC");
//
//    }
}