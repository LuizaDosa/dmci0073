package dmci0073MV.repository.repoMock;

import dmci0073MV.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTestWBT {
    Carte c1, c2, c3, c4;
    static CartiRepoMock repo;
    static List<Carte> carti;
    static List<Carte> cartiGasite;

    @BeforeClass
    public static void setup() {
        repo=new CartiRepoMock();
        carti=new ArrayList<Carte>();
        cartiGasite=new ArrayList<Carte>();
    }

    @Before
    public void setUp() throws Exception{
        List<String> autoriC1=new ArrayList<>(Arrays.asList("Mircea Cartarescu"));
        List<String> autoriC2=new ArrayList<>(Arrays.asList("Vlad"));
        List<String> autoriC3=new ArrayList<>(Arrays.asList("Vlad Popa"));
        List<String> autoriC4=new ArrayList<>(Arrays.asList("Mihai Eminescu"));

        List<String> cuvinteCheieC1=new ArrayList<>(Arrays.asList("nisip"));
        List<String> cuvinteCheieC2=new ArrayList<>(Arrays.asList("pamant","Eminescu"));
        List<String> cuvinteCheieC3=new ArrayList<>(Arrays.asList("om"));
        List<String> cuvinteCheieC4=new ArrayList<>(Arrays.asList("lume"));

        c1=new Carte("Era Noastra",autoriC1,"2000","Litera",cuvinteCheieC1);
        c2=new Carte("Alta carte",autoriC2,"2010","Corint",cuvinteCheieC2);
        c3=new Carte("Era Noastra",autoriC3,"2008","Humanitas",cuvinteCheieC3);
        c4=new Carte("Era Noastra",autoriC4,"2000","Corint",cuvinteCheieC4);


    }

    @After
    public void tearDown() throws Exception {
        c1=null;
        c2=null;
        c3=null;
        c4=null;

        System.out.println("inAfterTest");

    }

    @Test
    public void cautaCarteListaNull() {
        List<Carte> lista = repo.cautaCarte("Maria");
        assert lista.size() == 0;
    }

    @Test
    public void testCautaCarteListaNenula(){
        repo.adaugaCarte(c1);
        List<Carte> lista = repo.cautaCarte("Mircea");
        assert lista.size() == 1;
    }

    @Test
    public void testCautaCarteListaMaiLunga(){
       repo.adaugaCarte(c2);
        repo.adaugaCarte(c3);
        repo.adaugaCarte(c4);
        List<Carte> list = repo.cautaCarte("Vlad");
        assert list.size() == 2;
    }

}